/*--------------------------------------------------------------------*-

    main.c (Released 2019-04)

--------------------------------------------------------------------

    main file for Co-operative - Time Driven System - Project for LPC1769.

    See readme.txt for project information.

-*--------------------------------------------------------------------*/


// Project header
#include <stdint.h>
#include "chip_eint.h"
#include "main.h"
#include "motor_pap_driver.h"
#include"botonera.h"
#include "motor_pwm.h"
#include "lcd.h"
#include "task_motor.h"
#include "tp.h"
#include "botonera.h"
//#include "ff.h"
//#include "CAPI_diskio.h"
//#include "CAPI_capa_SPI.h"
#include"sd.h"
#include<string.h>
estado_cnc ESTADO_CNC=CNC_inicializacion;

int main(void)
{
	ESTADO_CNC=CNC_standby;
	// Check mode, add tasks to schedule
	//inicializo los motores
	SYSTEM_Init();
	init_iexterna();
	init_motor(0);
	init_motor_timer();
	Init_lcd_Hardware();
	Init_Botonera();

	//inicializo el PWM
	//init_motor_pwm();


	SCH_Add_Task(Task_Init,1,1,10000,2);
	SCH_Add_Task(Task_calibracion_z,2,1,10000,2);
	SCH_Add_Task(Task_Perforacion,3,1,10000,2);
	SCH_Add_Task(Task_Debounce,4,1, 10000,2);
	SCH_Add_Task(Task_fresadora,5,1,10000,2);
	SCH_Add_Task(Task_CargarArchivo, 6, 1,10000,2);
	// Start the scheduler
    SCH_Start();



    while(1)
    {

    	SCH_Dispatch_Tasks();
    }


    return (RETURN_FAULT);
}


/*------------------------------------------------------------------*-
  ---- END OF FILE -------------------------------------------------
-*------------------------------------------------------------------*/

