/*--------------------------------------------------------------------*-

    task-car_wash_controller_lpc1769.c (Released 2019-04)

  --------------------------------------------------------------------

    Simple CAR WASH CONTROLLER task for LPC1769.
	[Read Input on LPCXpresso baseboard.]

    Simple car wash controller code.

    - See "Car Wash Cortroller.jpg" for details.

-*--------------------------------------------------------------------*/


// Project header
#include "task_motor.h"
#include "motor_pap_driver.h"
#include "../main/main.h"
#include "chip_eint.h"
#include "botonera.h"
#include "lcd.h"
#include "motor_pwm.h"
#include "CAPI_capa_SPI.h"
#include "CAPI_diskio.h"

#include "ff.h"
//#define Z_BAJAR 520


extern uint8_t volatile CNC_BUFFER_EMPTY,CNC_buffer_full;
extern uint32_t volatile indice_CNC_out,indice_CNC_in;

extern uint8_t boton_der,boton_izq,boton_ok;

// ------ Private data type ----------------------------------------
// Possible system states

extern estado_cnc ESTADO_CNC;
estado_inicio ESTADO_INICIO;


// ------ Private variable -----------------------------------------
/*
static eSystem_state System_state_G;
static uint32_t Time_in_state_G;
*/

uint32_t Z_BAJAR=0;
uint8_t vel_pwm=50;

void Task_Init(void)
{
	//LeerConfigSD();
	//static estado_inicio ESTADO_INICIO;
if(ESTADO_CNC==CNC_standby)
	{
	if(boton_ok)
	{
		lcd_boton_ok();
		boton_ok=0;
		ESTADO_CNC= CNC_inicializacion;
	}
	}

if(ESTADO_CNC==CNC_inicializacion)
{
switch (ESTADO_INICIO)
		{
case INICIO_Z:
				set_motor(MOTORZ,HORARIO,700,100000); //260 pasos avanza 1cm
				/*NVIC_ClearPendingIRQ(EINT0_IRQn);
				NVIC_EnableIRQ(EINT0_IRQn);*/
				ESTADO_INICIO=INICIO_XY;
				break;
case INICIO_XY:
				if(motor_pap_getstop(MOTORZ))
					{
				set_motor(MOTORX,HORARIO,700,100000); //260 pasos avanza 1cm
				set_motor(MOTORY,HORARIO,700,100000);
				/*NVIC_ClearPendingIRQ(EINT1_IRQn);
								NVIC_EnableIRQ(EINT1_IRQn);
								NVIC_ClearPendingIRQ(EINT2_IRQn);
								NVIC_EnableIRQ(EINT2_IRQn);*/
				ESTADO_INICIO=INICIO_FIN;
					}
				break;


case INICIO_FIN:
			if(motor_pap_getstop(MOTORZ)&& motor_pap_getstop(MOTORY) && motor_pap_getstop(MOTORX))
			{
				ESTADO_INICIO=INICIO_Z;
				motor_set_zero();
			//	ESTADO_CNC=CNC_fresadora;
				ESTADO_CNC=CNC_calibracion_z;
				motor_set_zero();
				lcd_pantalla_botones();
			}
				break;
		}
	}
}

void Task_Perforacion(void)
{
	static estado_perforacion ESTADO_PERFORACION=CNC_xy;
	static position_xyz_t *auxiliar;

if(ESTADO_CNC==CNC_perforacion)
{
	switch (ESTADO_PERFORACION)
	{
	case CNC_xy:
			if(motor_pap_getstop(MOTORZ) && !CNC_BUFFER_EMPTY)
			{
				auxiliar=pop_buffer_CNC();
				mover_motor(MOTORX,auxiliar->steps_x, 800);
				mover_motor(MOTORY,auxiliar->steps_y, 800);
				ESTADO_PERFORACION=CNC_mov_z_down;
			}
			break;
	case CNC_mov_z_down:
		{
			if( (motor_pap_getstop(MOTORX)) && (motor_pap_getstop(MOTORY)) )
				{
					mover_motor(MOTORZ,Z_BAJAR,800);
					ESTADO_PERFORACION=CNC_mov_z_up;
	        	}
			break;
		}
	case CNC_mov_z_up:
		{
			if( motor_pap_getstop(MOTORZ))
			{
				mover_motor(MOTORZ,0, 800);
				ESTADO_PERFORACION=CNC_xy;
				}
			break;
			}
		}
	}
}


void Task_calibracion_z(void)

{	/* ESTA TAREA NOS PERMITE DESDE LOS COMANDOS, CALIBRAR LA POSICIÓN
		DEL EJE Z*/
	if(ESTADO_CNC==CNC_calibracion_z)
	{

		//static menu calib_z=BOTON_A;
		static uint8_t a=0, b=0, c=0;



			if(boton_der)
						{
						lcd_puntero_a_clibz();
						boton_der=0;
						if(motor_pap_getstop(MOTORZ)) set_motor(MOTORZ,HORARIO,700,10);
						a++;
						}
			if(boton_ok)
						{

						lcd_puntero_b_clibz();
						Z_BAJAR=motor_pap_getsteps(MOTORZ);
						ESTADO_CNC=CNC_fresadora;

						//agrego esto para que luego de presionado una vez no siga entrando
							boton_ok=0;
							lcd_menu_fresadora();
						}
			if(boton_izq)
						{
						c++;
						lcd_puntero_c_clibz();
						if(motor_pap_getstop(MOTORZ)) set_motor(MOTORZ,AHORARIO,700,10);
						boton_izq=0;
						}

			if(a)
						{
							b++;
								if(b>=5)
								{
									a=0;
									b=0;
									lcd_puntero_borrar();
								}
						}
			if(c)
						{
							b++;
								if(b>=5)
								{
									c=0;
									b=0;
									lcd_puntero_borrar();
								}
						}

	}

}

void Task_fresadora(void)
	{
		/* ESTA TAREA NOS PERMITE DESDE LOS COMANDOS, CALIBRAR LA VELOCIDADDEL PWM*/
	if(ESTADO_CNC==CNC_fresadora)
		{
			//static menu calib_z=BOTON_A;

		static uint8_t a=0, b=0, c=0;

				if(boton_der)
							{
								lcd_puntero_a_clibz();
								vel_pwm+=10;
								if(vel_pwm>101) vel_pwm=100;
								vel_motor_pwm(vel_pwm);
								boton_der=0;
								a++;
							}
				if(boton_ok)
							{
							lcd_puntero_b_clibz();
							ESTADO_CNC=CNC_CARGAR;
							boton_ok=0;
							}
				if(boton_izq)
							{
								vel_pwm-=10;
								if(vel_pwm<9) vel_pwm=0;
								vel_motor_pwm(vel_pwm);
								lcd_puntero_c_clibz();
								boton_izq=0;
								c++;
							}
					if(a)
							{
								b++;
								if(b>=10)
								{
									a=0;
									b=0;
									lcd_puntero_borrar();
								}
							}
					if(c)
							{
								b++;
								if(b>=10)
								{
									b=0;
									c=0;
									lcd_puntero_borrar();
								}
							}
	}
}


void Task_CargarArchivo(void)
{
	if(ESTADO_CNC==CNC_CARGAR)
	{
		uint8_t 	datos[300];
		uint32_t i=0,auxx=0,auxy=0,auxz=0,d=1;
		/*
		char 	nombreArchivo[20] = "0:hola.txt";
		FATFS 	fs[1];
		FIL 	handleArchivo;
		FRESULT resultado;

		f_mount(0, &fs[0]);
		resultado = f_open(&handleArchivo, nombreArchivo, FA_OPEN_EXISTING | FA_READ);

		if(resultado == FR_OK)
		{
			f_gets(datos,64,&handleArchivo);
		}
		f_close(&handleArchivo);
		f_mount(0, NULL);*/
		while(datos[i]!=0)
		{
			while(datos[i]!=',')
			{
				if(datos[i]>='0'&& datos[i]<='9')
				{
				auxx=auxx+(datos[i]-48)*d;
				d*=10;
				i++;
				}
				else return;

			}
			i++;
			d=1;
			while(datos[i]!=',')
					{
						if(datos[i]>='0'&& datos[i]<='9')
						{
						auxy=auxy+(datos[i]-48)*d;
						d*=10;
						i++;
						}
						else return;
					}
			i++;
			d=1;
			while(datos[i]!=',')
						{
							if(datos[i]>='0'&& datos[i]<='9')
							{
							auxz=auxz+(datos[i]-48)*d;
							d*=10;
							i++;
							}
							else return;
						}
			if(datos[i]=='\n')
								push_buffer_CNC(auxx, auxy, auxz)	;
			else return;
			i++;
			d=1;

		}
	}
}

void EINT0_IRQHandler(void)   //MOTORZ
	{
	EXTINT = EXTINT | (0x01<<EINT0);
	if(ESTADO_CNC==CNC_inicializacion&&ESTADO_INICIO==INICIO_XY)
		set_motor(MOTORZ,AHORARIO,700,100); //inicializacion avanza 100pasos
	/*
	else
		{
			motor[0].cant_pasos=0;
			motor[1].cant_pasos=0;
			motor[2].cant_pasos=0;
			CNC_BUFFER_EMPTY=1;
			CNC_buffer_full=0;
			ESTADO_CNC=CNC_error;
		}
*/
	}

void EINT1_IRQHandler(void)    //MOTORX
	{

		EXTINT = EXTINT | (0x01<<EINT1);
		if(ESTADO_CNC==CNC_inicializacion&&ESTADO_INICIO==INICIO_FIN)
			set_motor(MOTORX,AHORARIO,700,100); //inicializacion avanza 100pasos
		/*else
		{
			motor[0].cant_pasos=0;
			motor[1].cant_pasos=0;
			motor[2].cant_pasos=0;
			CNC_BUFFER_EMPTY=1;
			CNC_buffer_full=0;
			ESTADO_CNC=CNC_error;
		}
	*/
	}
void EINT2_IRQHandler(void)    //MOTORY
	{
	EXTINT = EXTINT | (0x01<<EINT2);
	if(ESTADO_CNC==CNC_inicializacion&&ESTADO_INICIO==INICIO_FIN)
		set_motor(MOTORY,AHORARIO,700,100); //inicializacion avanza 100pasos
	/*
	else
		{
			motor[0].cant_pasos=0;
			motor[1].cant_pasos=0;
			motor[2].cant_pasos=0;
			CNC_BUFFER_EMPTY=1;
			CNC_buffer_full=0;
			ESTADO_CNC=CNC_error;
		}
	*/
	}



/*------------------------------------------------------------------*-
  ---- END OF FILE -------------------------------------------------
-*------------------------------------------------------------------*/
