//#include <stdint.h>
#include "chip.h"
#include "settings.h"
//#include "main.h"
#include "lcd.h"
#include "lcd_mi0283qt9.h"
#include "tp.h"

uint_least32_t get_ms(void);


//adaptacion a SPI

#ifdef LCD_MI0283QT9


#define RST         	1,18
#define CS	          	1,21
#define RS		 		1,24
#define WR	 	        1,28
#define RD            	1,27

#define D0				2,1
#define D1				2,2
#define D2				2,3
#define D3				2,4
#define D4				2,5
#define D5				2,6
#define D6				2,7
#define D7				2,8

/*
#define SPI_SSEL1 0,6
#define SPI_SCK1 0,7
#define SPI_MISO1 0,8
#define SPI_MOSI1 0,9
*/
void INIT_PINS()
{
	//uint32_t i;

	Chip_IOCON_PinMuxSet(LPC_IOCON, RST, IOCON_FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON, RS, IOCON_FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON, CS, IOCON_FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON, RD, IOCON_FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON, WR, IOCON_FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON, D0, IOCON_FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON, D1, IOCON_FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON, D2, IOCON_FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON, D3, IOCON_FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON, D4, IOCON_FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON, D5, IOCON_FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON, D6, IOCON_FUNC0);
	Chip_IOCON_PinMuxSet(LPC_IOCON, D7, IOCON_FUNC0);


	Chip_GPIO_SetPinDIROutput(LPC_GPIO,RST);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, RS);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, RD);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, WR);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, CS);

	Chip_GPIO_SetPinDIROutput(LPC_GPIO, D0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, D1);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, D2);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, D3);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, D4);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, D5);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, D6);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, D7);






	/*Habilito el controlador de SPI
	for(i=0;i<500000;i++);
	Chip_IOCON_PinMuxSet(LPC_IOCON,SPI_SSEL1,IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,SPI_SSEL1);
	Chip_GPIO_SetPinOutLow(LPC_GPIO,SPI_SSEL1);
	Chip_IOCON_PinMuxSet(LPC_IOCON,SPI_SCK1,IOCON_FUNC2);
	Chip_IOCON_PinMuxSet(LPC_IOCON,SPI_MISO1,IOCON_FUNC2);
	Chip_IOCON_PinMuxSet(LPC_IOCON,SPI_MOSI1,IOCON_FUNC2);
	Chip_SSP_Init(LPC_SSP1);
	Chip_SSP_SetFormat(LPC_SSP1,SSP_BITS_8,SSP_FRAMEFORMAT_SPI,SSP_CLOCK_CPHA0_CPOL0);
	Chip_SSP_SetBitRate(LPC_SSP1,100000);
	Chip_SSP_Enable(LPC_SSP1);
	*/

}


/*
__attribute__((always_inline)) __INLINE void lcd_drawstop(void)
{
  //lcd_disable();

  return;
}
*/

void lcd_draw(uint_least16_t color)
{
  return lcd_wrdata16(color);
}


void lcd_drawstart(void)
{
  //lcd_enable();

  lcd_wrcmd8(LCD_CMD_WRITE);

  return;
}


void lcd_setarea(uint_least16_t x0, uint_least16_t y0, uint_least16_t x1, uint_least16_t y1)
{
  //lcd_enable();

  lcd_wrcmd8(LCD_CMD_COLUMN);
  lcd_wrdata16(y0);
  lcd_wrdata16(y1);

  lcd_wrcmd8(LCD_CMD_PAGE);
  lcd_wrdata16(x0);
  lcd_wrdata16(x1);

  //lcd_disable();

  return;
}


uint_least32_t lcd_setbias(uint_least16_t o)
{
  uint_least32_t w, h;
  uint_least8_t param;

  #define MEM_Y   (7) //MY row address order
  #define MEM_X   (6) //MX column address order 
  #define MEM_V   (5) //MV row / column exchange 
  #define MEM_L   (4) //ML vertical refresh order
  #define MEM_H   (2) //MH horizontal refresh order
  #define MEM_BGR (3) //RGB-BGR Order 

  switch(o)
  {
    default:
    case 0:
    //case 36:
    //case 360:
      w     = LCD_WIDTH;
      h     = LCD_HEIGHT;
      param = (1<<MEM_BGR) | (1<<MEM_X) | (1<<MEM_Y);
      break;
    case 90:
      w     = LCD_HEIGHT;
      h     = LCD_WIDTH;
      param = (1<<MEM_BGR) | (1<<MEM_X) | (1<<MEM_V);
      break;
    case 180:
      w     = LCD_WIDTH;
      h     = LCD_HEIGHT;
      param = (1<<MEM_BGR) | (1<<MEM_L);
      break;
    case 270:
      w     = LCD_HEIGHT;
      h     = LCD_WIDTH;
      param = (1<<MEM_BGR) | (1<<MEM_Y) | (1<<MEM_V);
      break;
  }

  //lcd_enable();

  lcd_wrcmd8(LCD_CMD_MEMACCESS_CTRL);
  lcd_wrdata8(param);

  lcd_setarea(0, 0, w-1, h-1);

  //lcd_disable();

  return (w<<16) | (h<<0);
}


void lcd_invert(uint_least8_t on)
{
  lcd_enable();

  if(on == 0)
  {
     lcd_wrcmd8(LCD_CMD_INV_OFF);
  }
  else
  {
     lcd_wrcmd8(LCD_CMD_INV_ON);
  }

  lcd_disable();

  return;
}


void lcd_power(uint_least8_t on)
{
  lcd_enable();

  if(on == 0)
  {
    lcd_wrcmd8(LCD_CMD_DISPLAY_OFF);
    delay_ms(20);
    lcd_wrcmd8(LCD_CMD_SLEEPIN);
    delay_ms(120);
  }
  else
  {
    lcd_wrcmd8(LCD_CMD_SLEEPOUT);
    delay_ms(120);
    lcd_wrcmd8(LCD_CMD_DISPLAY_ON);
    delay_ms(20);
  }

  lcd_disable();

  return;
}


void lcd_reset(void)
{
//	uint_least32_t s = 0;

    //init pins
    INIT_PINS();

    //hardware reset
    //GPIO_CLRPIN(LCD_PORT, RST_PIN);
    Chip_GPIO_SetPinOutLow(LPC_GPIO,RST);
    delay_ms(20);

    Chip_GPIO_SetPinOutHigh(LPC_GPIO,RST);
    //GPIO_SETPIN(LCD_PORT, RST_PIN);
    delay_ms(120);

  	lcd_enable();

    lcd_wrcmd8(0x01);//soft reset
  	delay_ms(1000);

  	//power control A
  	lcd_wrcmd8(0xCB);
  	lcd_wrdata8(0x39);
  	lcd_wrdata8(0x2C);
  	lcd_wrdata8(0x00);
  	lcd_wrdata8(0x34);
  	lcd_wrdata8(0x02);

  	//power control B
  	lcd_wrcmd8(0xCF);
  	lcd_wrdata8(0x00);
  	lcd_wrdata8(0xC1);
  	lcd_wrdata8(0x30);

  	//driver timing control A
  	lcd_wrcmd8(0xE8);
  	lcd_wrdata8(0x85);
  	lcd_wrdata8(0x00);
  	lcd_wrdata8(0x78);

  	//driver timing control B
  	lcd_wrcmd8(0xEA);
  	lcd_wrdata8(0x00);
  	lcd_wrdata8(0x00);

  	//power on sequence control
  	lcd_wrcmd8(0xED);
  	lcd_wrdata8(0x64);
  	lcd_wrdata8(0x03);
  	lcd_wrdata8(0x12);
  	lcd_wrdata8(0x81);

  	//pump ratio control
  	lcd_wrcmd8(0xF7);
  	lcd_wrdata8(0x20);

  	//power control,VRH[5:0]
  	lcd_wrcmd8(0xC0);
  	lcd_wrdata8(0x23);

  	//Power control,SAP[2:0];BT[3:0]
  	lcd_wrcmd8(0xC1);
  	lcd_wrdata8(0x10);

  	//vcm control
  	lcd_wrcmd8(0xC5);
  	lcd_wrdata8(0x3E);
  	lcd_wrdata8(0x28);

  	//vcm control 2
  	lcd_wrcmd8(0xC7);
  	lcd_wrdata8(0x86);

  	//memory access control
  	lcd_wrcmd8(0x36);
  	lcd_wrdata8(0x48);

  	//pixel format
  	lcd_wrcmd8(0x3A);
  	lcd_wrdata8(0x55);

  	//frameration control,normal mode full colours
  	lcd_wrcmd8(0xB1);
  	lcd_wrdata8(0x00);
  	lcd_wrdata8(0x18);

  	//display function control
  	lcd_wrcmd8(0xB6);
  	lcd_wrdata8(0x08);
  	lcd_wrdata8(0x82);
  	lcd_wrdata8(0x27);

  	//3gamma function disable
  	lcd_wrcmd8(0xF2);
  	lcd_wrdata8(0x00);

  	//gamma curve selected
  	lcd_wrcmd8(0x26);
  	lcd_wrdata8(0x01);

  	//set positive gamma correction
  	lcd_wrcmd8(0xE0);
  	lcd_wrdata8(0x0F);
  	lcd_wrdata8(0x31);
  	lcd_wrdata8(0x2B);
  	lcd_wrdata8(0x0C);
  	lcd_wrdata8(0x0E);
  	lcd_wrdata8(0x08);
  	lcd_wrdata8(0x4E);
  	lcd_wrdata8(0xF1);
  	lcd_wrdata8(0x37);
  	lcd_wrdata8(0x07);
  	lcd_wrdata8(0x10);
  	lcd_wrdata8(0x03);
  	lcd_wrdata8(0x0E);
  	lcd_wrdata8(0x09);
  	lcd_wrdata8(0x00);

  	//set negative gamma correction
  	lcd_wrcmd8(0xE1);
  	lcd_wrdata8(0x00);
  	lcd_wrdata8(0x0E);
  	lcd_wrdata8(0x14);
  	lcd_wrdata8(0x03);
  	lcd_wrdata8(0x11);
  	lcd_wrdata8(0x07);
  	lcd_wrdata8(0x31);
  	lcd_wrdata8(0xC1);
  	lcd_wrdata8(0x48);
  	lcd_wrdata8(0x08);
  	lcd_wrdata8(0x0F);
  	lcd_wrdata8(0x0C);
  	lcd_wrdata8(0x31);
  	lcd_wrdata8(0x36);
  	lcd_wrdata8(0x0F);

  	//exit sleep
  	lcd_wrcmd8(0x11);
  	delay_ms(120);
  	//display on
  	lcd_wrcmd8(0x29);

  return;
}
/*
void SendDatoSPI(uint8_t dato)
{
	uint16_t dummy;
	uint16_t datoSerie;
	Chip_SSP_DATA_SETUP_T datospi;
	datoSerie = dato;
	datospi.rx_cnt=0;
	datospi.tx_cnt=0;
	datospi.rx_data=&dummy;
	datospi.tx_data=&datoSerie;
	datospi.length= 1;
	Chip_GPIO_SetPinOutLow(LPC_GPIO,SPI_SSEL1);
	Chip_SSP_RWFrames_Blocking(LPC_SSP1,&datospi);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO,SPI_SSEL1);
}
*/

void lcd_wrdata16(uint_least16_t data)
{
  //GPIO_SETPIN(LCD_PORT, RS_PIN); //data
  Chip_GPIO_SetPinOutHigh(LPC_GPIO, RS); //rs high cuando es data
  //delay_ms(1);
  lcd_wrdata8(data>>8);
  lcd_wrdata8(data);
  return;
}


void lcd_wrdata8(uint_least8_t data)
{
  Chip_GPIO_SetPinOutHigh(LPC_GPIO,RS); //rs high cuando es data
  Chip_GPIO_SetPinOutHigh(LPC_GPIO,RD);
  lcd_enable();
  //delay_ms(1);
  Chip_GPIO_SetPinState(LPC_GPIO,WR,0);


  Chip_GPIO_SetPinState(LPC_GPIO,D0,(data)&(1<<0));
  Chip_GPIO_SetPinState(LPC_GPIO,D1,(data)&(1<<1));
  Chip_GPIO_SetPinState(LPC_GPIO,D2,(data)&(1<<2));
  Chip_GPIO_SetPinState(LPC_GPIO,D3,(data)&(1<<3));
  Chip_GPIO_SetPinState(LPC_GPIO,D4,(data)&(1<<4));
  Chip_GPIO_SetPinState(LPC_GPIO,D5,(data)&(1<<5));
  Chip_GPIO_SetPinState(LPC_GPIO,D6,(data)&(1<<6));
  Chip_GPIO_SetPinState(LPC_GPIO,D7,(data)&(1<<7));

  Chip_GPIO_SetPinState(LPC_GPIO,WR,1);

  //SendDatoSPI(data);
  lcd_disable();
  return;
}


void lcd_wrcmd8(uint_least8_t cmd)
{
	Chip_GPIO_SetPinOutLow(LPC_GPIO,RS); //rs low cuando es cmd
	//Chip_GPIO_SetPinOutHigh(LPC_GPIO,RS); //rs high cuando es data
	Chip_GPIO_SetPinOutHigh(LPC_GPIO,RD);
	  lcd_enable();


	  Chip_GPIO_SetPinState(LPC_GPIO,WR,0);
	  //delay_ms(1);
	  Chip_GPIO_SetPinState(LPC_GPIO,D0,(cmd)&(1<<0));
	  Chip_GPIO_SetPinState(LPC_GPIO,D1,(cmd)&(1<<1));
	  Chip_GPIO_SetPinState(LPC_GPIO,D2,(cmd)&(1<<2));
	  Chip_GPIO_SetPinState(LPC_GPIO,D3,(cmd)&(1<<3));
	  Chip_GPIO_SetPinState(LPC_GPIO,D4,(cmd)&(1<<4));
	  Chip_GPIO_SetPinState(LPC_GPIO,D5,(cmd)&(1<<5));
	  Chip_GPIO_SetPinState(LPC_GPIO,D6,(cmd)&(1<<6));
	  Chip_GPIO_SetPinState(LPC_GPIO,D7,(cmd)&(1<<7));

	  Chip_GPIO_SetPinState(LPC_GPIO,WR,1);
	  //SendDatoSPI(data);

	  lcd_disable();
	return;
}

void lcd_disable(void)
{
  //  GPIO_SETPIN(LCD_PORT, CS_PIN);
  Chip_GPIO_SetPinOutHigh(LPC_GPIO, CS);
  return;
}


void lcd_enable(void)
{
  Chip_GPIO_SetPinOutLow(LPC_GPIO, CS);
  return;
}

void delay_ms(uint_least32_t delay)
{
	uint32_t i;
  for(i=0;i<(delay*13000);i++);

  return;
}

void Init_lcd_Hardware(void)
{
	//uint32_t i;


	SystemCoreClockUpdate();

	Chip_GPIO_Init(LPC_GPIO);

	Chip_IOCON_Init(LPC_IOCON);
	//TP_HW_Disable();
	INIT_PINS();


	lcd_init();
	lcd_power(1);
	lcd_pantalla_utn();
	delay_ms(1000);
	lcd_pantalla_inicio();
	//lcd_pantalla_botones();

}



#endif //LCD_MI0283QT9
