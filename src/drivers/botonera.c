
#include <stdint.h>
#include "chip.h"
#include "botonera.h"

uint32_t boton_der,boton_izq,boton_ok;

void Init_Botonera(void)
{
	Chip_IOCON_Init(LPC_IOCON);
	Chip_GPIO_Init(LPC_GPIO);
	//INICIAZLIZO LOS PINES
	Chip_IOCON_PinMux(LPC_IOCON, BOT_DER,MD_PDN, IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON, BOT_IZQ,MD_PDN, IOCON_FUNC0);
	Chip_IOCON_PinMux(LPC_IOCON, BOT_OK,MD_PDN, IOCON_FUNC0);

	Chip_GPIO_SetPinDIRInput(LPC_GPIO, BOT_DER);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO, BOT_IZQ);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO, BOT_OK);

}

void Task_Debounce(void)
{
		static uint32_t i=0,j=0,k=0,act_der,act_izq,act_ok,ant_der,ant_izq,ant_ok;

	act_der=Chip_GPIO_GetPinState(LPC_GPIO, BOT_DER);
		if(act_der==ant_der &&  (act_der != 0)) //agrego condicion de que act_der sea distinto de cero
		{
		i++;
		if(i>MAX_DEB)
			{
			boton_der=act_der;
			i=0;
			}
		}
		else
			{
			ant_der=act_der;
			i=0;
			}


	act_izq=Chip_GPIO_GetPinState(LPC_GPIO, BOT_IZQ);
	if(act_izq==ant_izq &&  (act_izq != 0))
			{
			j++;
			if(j>MAX_DEB)
				{
				boton_izq=act_izq;
				j=0;
				}
			}
		else
			{
			ant_izq=act_izq;
			j=0;
			}


	act_ok=Chip_GPIO_GetPinState(LPC_GPIO, BOT_OK);
	if(act_ok==ant_ok&&  (act_ok != 0))
				{
				k++;
				if(k>MAX_DEB)
					{
					boton_ok=act_ok;
					k=0;
					}
				}
			else
				{
				ant_ok=act_ok;
				k=0;
				}

}
