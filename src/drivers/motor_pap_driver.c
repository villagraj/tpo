/*
 * motor_pap.c
 *
 *  Created on: 11 jul. 2018
 *      Author: Mauro
 */
/*!\file
 *Drivers de motores
 *
 */

#include "chip.h"
#include "motor_pap_driver.h"
stepper_m_t motor[3];


uint8_t volatile CNC_BUFFER_EMPTY=1,CNC_buffer_full=0;
uint32_t volatile indice_CNC_out=0,indice_CNC_in=0;


position_xyz_t buffer_CNC[1000];	/*!<buffer que se cargara con todas las posiciones xyz que debe realizar la maquina (buffer circular)*/
/*! \brief
*Buffer pasos simples
*	| A+ 	| B+	 | A-   |B-	  |
*	| ----: | :----: | :----|:----|
*	| 1 	| 0 	 | 0   	|0	  |
*	| 0		| 1	     | 0 	|0    |
*	| 0		| 0	     | 1 	|0    |
*	| 0		| 0	     | 0 	|1    |
*/
uint8_t steps_buf_s[4]={1,2,4,8};

/*! \brief
*Buffer pasos dobles
*	| A+ 	| B+	 | A-   |B-	  |
*	| ----: | :----: | :----|:----|
*	| 1 	| 1 	 | 0   	|0	  |
*	| 0		| 1	     | 1 	|0    |
*	| 0		| 0	     | 1 	|1    |
*	| 1		| 0	     | 0 	|1    |
*/
uint8_t steps_buf_d[4]={9,3,6,12};
/*! \brief
*Buffer pasos medios
*	| A+ 	| B+	 | A-   |B-	  |
*	| ----: | :----: | :----|:----|
*	| 1 	| 1 	 | 0   	|0	  |
*	| 0		| 1	     | 0 	|0    |
*	| 0		| 1	     | 1 	|0    |
*	| 0		| 0	     | 1 	|0    |
*	| 0		| 0	     | 1 	|1    |
*	| 0		| 0	     | 0 	|1    |
*	| 1		| 0	     | 0 	|1    |
*	| 1		| 0	     | 0 	|0    |
*/
uint8_t steps_buf_h[8]={8,9,1,3,2,6,4,12};
uint8_t steps_buf[8];
uint32_t timerFreq;

void stop_motor(uint8_t n_motor)
{
	if(!(motor[n_motor].flag_stop) & (motor[n_motor].steps_to_do!=0))
	{
	if(n_motor==MOTORX)Chip_TIMER_MatchDisableInt(LPC_TIMER1,MOTORX);
	if(n_motor==MOTORY)Chip_TIMER_MatchDisableInt(LPC_TIMER1,MOTORY);
	if(n_motor==MOTORZ)Chip_TIMER_MatchDisableInt(LPC_TIMER1,MOTORZ);

	if(n_motor==MOTORX)motor[MOTORX].flag_stop=1;
	if(n_motor==MOTORY)motor[MOTORY].flag_stop=1;
	if(n_motor==MOTORZ)motor[MOTORZ].flag_stop=1;
	}
}
void continue_motor(uint8_t n_motor)
 {
	 if(motor[n_motor].flag_stop & motor[n_motor].steps_to_do)
	 {
		if(n_motor==MOTORX)Chip_TIMER_MatchEnableInt(LPC_TIMER1,MOTORX);
		if(n_motor==MOTORY)Chip_TIMER_MatchEnableInt(LPC_TIMER1,MOTORY);
		if(n_motor==MOTORZ)Chip_TIMER_MatchEnableInt(LPC_TIMER1,MOTORZ);

		if(n_motor==MOTORX)motor[MOTORX].flag_stop=0;
		if(n_motor==MOTORY)motor[MOTORY].flag_stop=0;
		if(n_motor==MOTORZ)motor[MOTORZ].flag_stop=0;
	 }
 }
void dr_rotar_motorx()
{
	Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORX_E);
	if(!motor[MOTORX].steps_to_do) 			/*!si no hay mas pasos por realizar activo flag stop*/
				{
				motor[MOTORX].flag_stop=1;
				Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORX_E);
				Chip_TIMER_MatchDisableInt(LPC_TIMER1,MOTORX);
				return;
				}
	if(motor[MOTORX].flag_stop)
		{
		Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORX_E);
		Chip_TIMER_MatchDisableInt(LPC_TIMER1,MOTORX);
		return;
		}

	Chip_GPIO_SetPinState(LPC_GPIO,MOTORX_D0,(steps_buf[motor[MOTORX].steps_i])&(1<<0));
	Chip_GPIO_SetPinState(LPC_GPIO,MOTORX_D1,(steps_buf[motor[MOTORX].steps_i])&(1<<1));
	Chip_GPIO_SetPinState(LPC_GPIO,MOTORX_D2,(steps_buf[motor[MOTORX].steps_i])&(1<<2));
	Chip_GPIO_SetPinState(LPC_GPIO,MOTORX_D3,(steps_buf[motor[MOTORX].steps_i])&(1<<3));

	Chip_GPIO_SetPinOutHigh(LPC_GPIO,MOTORX_E);

	if(motor[MOTORX].direction==HORARIO)
		{
		motor[MOTORX].steps_i++;
		motor[MOTORX].steps--;
		if(motor[MOTORX].steps_i==4) motor[MOTORX].steps_i=0;
		}
	if(motor[MOTORX].direction==AHORARIO)
		{
		if(motor[MOTORX].steps_i==0) motor[MOTORX].steps_i=4;
		motor[MOTORX].steps_i--;
		motor[MOTORX].steps++;
		}
	motor[MOTORX].steps_to_do--;

	motor[MOTORX].pos_mm=(motor[MOTORX].steps/26);

}

void dr_rotar_motory()
{
	Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORY_E);
	if(!motor[MOTORY].steps_to_do) 			/*!si no hay mas pasos por realizar activo flag stop*/
				{
				motor[MOTORY].flag_stop=1;

				Chip_TIMER_MatchDisableInt(LPC_TIMER1,MOTORY);
				return;
				}
	if(motor[MOTORY].flag_stop)
		{
		Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORY_E);
		Chip_TIMER_MatchDisableInt(LPC_TIMER1,MOTORY);
		return;
		}

	Chip_GPIO_SetPinState(LPC_GPIO,MOTORY_D0,(steps_buf[motor[MOTORY].steps_i])&(1<<0));
	Chip_GPIO_SetPinState(LPC_GPIO,MOTORY_D1,(steps_buf[motor[MOTORY].steps_i])&(1<<1));
	Chip_GPIO_SetPinState(LPC_GPIO,MOTORY_D2,(steps_buf[motor[MOTORY].steps_i])&(1<<2));
	Chip_GPIO_SetPinState(LPC_GPIO,MOTORY_D3,(steps_buf[motor[MOTORY].steps_i])&(1<<3));

	Chip_GPIO_SetPinOutHigh(LPC_GPIO,MOTORY_E);

	if(motor[MOTORY].direction==HORARIO)
		{
		motor[MOTORY].steps_i++;
		motor[MOTORY].steps--;
		if(motor[MOTORY].steps_i==4) motor[MOTORY].steps_i=0;
		}
	if(motor[MOTORY].direction==AHORARIO)
		{
		if(motor[MOTORY].steps_i==0) motor[MOTORY].steps_i=4;
		motor[MOTORY].steps_i--;
		motor[MOTORY].steps++;
		}
	motor[MOTORY].steps_to_do--;

	motor[MOTORY].pos_mm=(motor[MOTORY].steps/26);
}

void dr_rotar_motorz()
{
	Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORZ_E);
	if(!motor[MOTORZ].steps_to_do) 			/*!si no hay mas pasos por realizar activo flag stop*/
				{
				motor[MOTORZ].flag_stop=1;

				Chip_TIMER_MatchDisableInt(LPC_TIMER1,MOTORZ);
				return;
				}
	if(motor[MOTORZ].flag_stop)
		{
		Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORZ_E);
		Chip_TIMER_MatchDisableInt(LPC_TIMER1,MOTORZ);
		return;
		}

	Chip_GPIO_SetPinState(LPC_GPIO,MOTORZ_D0,(steps_buf[motor[MOTORZ].steps_i])&(1<<0));
	Chip_GPIO_SetPinState(LPC_GPIO,MOTORZ_D1,(steps_buf[motor[MOTORZ].steps_i])&(1<<1));
	Chip_GPIO_SetPinState(LPC_GPIO,MOTORZ_D2,(steps_buf[motor[MOTORZ].steps_i])&(1<<2));
	Chip_GPIO_SetPinState(LPC_GPIO,MOTORZ_D3,(steps_buf[motor[MOTORZ].steps_i])&(1<<3));

	Chip_GPIO_SetPinOutHigh(LPC_GPIO,MOTORZ_E);

	if(motor[MOTORZ].direction==HORARIO)
		{
		motor[MOTORZ].steps_i++;
		motor[MOTORZ].steps--;
		if(motor[MOTORZ].steps_i==4) motor[MOTORZ].steps_i=0;
		}
	if(motor[MOTORZ].direction==AHORARIO)
		{
		if(motor[MOTORZ].steps_i==0) motor[MOTORZ].steps_i=4;
		motor[MOTORZ].steps_i--;
		motor[MOTORZ].steps++;
		}
	motor[MOTORZ].steps_to_do--;

	motor[MOTORZ].pos_mm=(motor[MOTORZ].steps/26);
}

inline uint8_t	set_motor(uint8_t n_motor,uint8_t direction ,uint32_t vel,uint32_t steps)
					{
					if (n_motor>=MAX_MOTOR) return -1;
					motor[n_motor].direction=direction;
					motor[n_motor].steps_to_do=steps;
					motor[n_motor].flag_stop=0;
					motor[n_motor].steps_i=0;
					motor[n_motor].vel=vel;
					if(n_motor==MOTORX && vel!=0 && steps!=0)
										{
										Chip_TIMER_SetMatch(LPC_TIMER1, MOTORX,Chip_TIMER_ReadCount(LPC_TIMER1)+ (timerFreq / (vel*4)));

										Chip_TIMER_MatchEnableInt(LPC_TIMER1, MOTORX);
										Chip_TIMER_Enable(LPC_TIMER1);//disparo timer
										return 1;
										}
					if(n_motor==MOTORY && vel!=0 && steps!=0)
										{
										Chip_TIMER_SetMatch(LPC_TIMER1, MOTORY, Chip_TIMER_ReadCount(LPC_TIMER1)+(timerFreq / (vel*4)));
										Chip_TIMER_MatchEnableInt(LPC_TIMER1, MOTORY);//disparo timer
										Chip_TIMER_Enable(LPC_TIMER1);
										return 1;
										}
					if(n_motor==MOTORZ && vel!=0 && steps!=0)
										{
										Chip_TIMER_SetMatch(LPC_TIMER1, MOTORZ, Chip_TIMER_ReadCount(LPC_TIMER1)+(timerFreq / (vel*4)));
										Chip_TIMER_MatchEnableInt(LPC_TIMER1, MOTORZ);//disparo timer
										Chip_TIMER_Enable(LPC_TIMER1);
										return 1;
										}
					if(vel==0) motor[n_motor].flag_stop=1;

					return 1;

					}
/**********************************************************************************************************/


//modificamos los timers 28/08

uint8_t init_motor_timer()
	{

	//Chip_TIMER_Init(LPC_TIMER3);
	//Chip_TIMER_Init(LPC_TIMER2);

	//NVIC_ClearPendingIRQ(TIMER3_IRQn);
	//NVIC_EnableIRQ(TIMER3_IRQn);

	NVIC_ClearPendingIRQ(TIMER1_IRQn);
	NVIC_EnableIRQ(TIMER1_IRQn);
	Chip_TIMER_Init(LPC_TIMER1);

	//NVIC_ClearPendingIRQ(TIMER2_IRQn);
	//NVIC_EnableIRQ(TIMER2_IRQn);

		//Chip_TIMER_Init(LPC_TIMER3);
		Chip_TIMER_Init(LPC_TIMER1);
		//Chip_TIMER_Init(LPC_TIMER2);

		//Chip_TIMER_Disable(LPC_TIMER2);
		Chip_TIMER_Disable(LPC_TIMER1);
		//Chip_TIMER_Disable(LPC_TIMER3);

		timerFreq = Chip_Clock_GetSystemClockRate();

	/*	//Chip_TIMER_Reset(LPC_TIMER3);
		Chip_TIMER_MatchEnableInt(LPC_TIMER3, 1);//habilita irq match 1 timer 0
		Chip_TIMER_ResetOnMatchEnable(LPC_TIMER3, 1);//restea cuenta al llegar al match
		//Chip_TIMER_Enable(LPC_TIMER0);//disparo timer
*/
		Chip_TIMER_Reset(LPC_TIMER1);
		Chip_TIMER_MatchDisableInt(LPC_TIMER1, MOTORX);
		Chip_TIMER_MatchDisableInt(LPC_TIMER1, MOTORY);
		Chip_TIMER_MatchDisableInt(LPC_TIMER1, MOTORZ);//habilita irq match 1 timer 0
		//Chip_TIMER_SetMatch(LPC_TIMER1, 1, (timerFreq / (3000*4)));//Tickrate cuantas veces por seg quiero que interrumpa mi timer-match*4
		Chip_TIMER_ResetOnMatchDisable(LPC_TIMER1, MOTORX);//restea cuenta al llegar al match
		Chip_TIMER_ResetOnMatchDisable(LPC_TIMER1, MOTORY);
		Chip_TIMER_ResetOnMatchDisable(LPC_TIMER1, MOTORZ);
		//Chip_TIMER_Enable(LPC_TIMER1);
/*
		Chip_TIMER_Reset(LPC_TIMER2);
		Chip_TIMER_MatchEnableInt(LPC_TIMER2, 1);//habilita irq match 1 timer 0
		//Chip_TIMER_SetMatch(LPC_TIMER2, 1, (timerFreq / (3000*4)));//Tickrate cuantas veces por seg quiero que interrumpa mi timer-match*4
		Chip_TIMER_ResetOnMatchEnable(LPC_TIMER2, 1);//restea cuenta al llegar al match
		*/
		return 1;
	}


uint8_t init_motor(uint8_t sdh)
	{
	uint8_t i;
	if(sdh==S_STEPS)
		for(i=0;i<4;i++)
			steps_buf[i]=steps_buf_s[i];

	Chip_GPIO_Init(LPC_GPIO);
	Chip_IOCON_Init(LPC_IOCON);

	Chip_IOCON_PinMux(LPC_IOCON,MOTORX_D0,MD_PLN,IOCON_FUNC0);
	Chip_IOCON_EnableOD(LPC_IOCON, MOTORX_D0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,MOTORX_D0);

	Chip_IOCON_PinMux(LPC_IOCON,MOTORX_D1,MD_PLN,IOCON_FUNC0);
	Chip_IOCON_EnableOD(LPC_IOCON, MOTORX_D1);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,MOTORX_D1);

	Chip_IOCON_PinMux(LPC_IOCON,MOTORX_D2,MD_PLN,IOCON_FUNC0);
	Chip_IOCON_EnableOD(LPC_IOCON, MOTORX_D2);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,MOTORX_D2);

	Chip_IOCON_PinMux(LPC_IOCON,MOTORX_D3,MD_PLN,IOCON_FUNC0);
	Chip_IOCON_EnableOD(LPC_IOCON, MOTORX_D3);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,MOTORX_D3);

	Chip_IOCON_PinMux(LPC_IOCON,MOTORX_E,MD_PLN,IOCON_FUNC0);
	Chip_IOCON_EnableOD(LPC_IOCON, MOTORX_E);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,MOTORX_E);
	/****************************************************************/
	Chip_IOCON_PinMux(LPC_IOCON,MOTORY_D0,MD_PLN,IOCON_FUNC0);
		Chip_IOCON_EnableOD(LPC_IOCON, MOTORY_D0);
		Chip_GPIO_SetPinDIROutput(LPC_GPIO,MOTORY_D0);

		Chip_IOCON_PinMux(LPC_IOCON,MOTORY_D1,MD_PLN,IOCON_FUNC0);
		Chip_IOCON_EnableOD(LPC_IOCON, MOTORY_D1);
		Chip_GPIO_SetPinDIROutput(LPC_GPIO,MOTORY_D1);

		Chip_IOCON_PinMux(LPC_IOCON,MOTORY_D2,MD_PLN,IOCON_FUNC0);
		Chip_IOCON_EnableOD(LPC_IOCON, MOTORY_D2);
		Chip_GPIO_SetPinDIROutput(LPC_GPIO,MOTORY_D2);

		Chip_IOCON_PinMux(LPC_IOCON,MOTORY_D3,MD_PLN,IOCON_FUNC0);
		Chip_IOCON_EnableOD(LPC_IOCON, MOTORY_D3);
		Chip_GPIO_SetPinDIROutput(LPC_GPIO,MOTORY_D3);

		Chip_IOCON_PinMux(LPC_IOCON,MOTORY_E,MD_PLN,IOCON_FUNC0);
		Chip_IOCON_EnableOD(LPC_IOCON, MOTORY_E);
		Chip_GPIO_SetPinDIROutput(LPC_GPIO,MOTORY_E);
		/****************************************************************/
		Chip_IOCON_PinMux(LPC_IOCON,MOTORZ_D0,MD_PLN,IOCON_FUNC0);
			Chip_IOCON_EnableOD(LPC_IOCON, MOTORZ_D0);
			Chip_GPIO_SetPinDIROutput(LPC_GPIO,MOTORZ_D0);

			Chip_IOCON_PinMux(LPC_IOCON,MOTORZ_D1,MD_PLN,IOCON_FUNC0);
			Chip_IOCON_EnableOD(LPC_IOCON, MOTORZ_D1);
			Chip_GPIO_SetPinDIROutput(LPC_GPIO,MOTORZ_D1);

			Chip_IOCON_PinMux(LPC_IOCON,MOTORZ_D2,MD_PLN,IOCON_FUNC0);
			Chip_IOCON_EnableOD(LPC_IOCON, MOTORZ_D2);
			Chip_GPIO_SetPinDIROutput(LPC_GPIO,MOTORZ_D2);

			Chip_IOCON_PinMux(LPC_IOCON,MOTORZ_D3,MD_PLN,IOCON_FUNC0);
			Chip_IOCON_EnableOD(LPC_IOCON, MOTORZ_D3);
			Chip_GPIO_SetPinDIROutput(LPC_GPIO,MOTORZ_D3);

			Chip_IOCON_PinMux(LPC_IOCON,MOTORZ_E,MD_PLN,IOCON_FUNC0);
			Chip_IOCON_EnableOD(LPC_IOCON, MOTORZ_E);
			Chip_GPIO_SetPinDIROutput(LPC_GPIO,MOTORZ_E);
			/****************************************************************/
			//pongo como entrada los 2 pines que no funcionan...
			Chip_IOCON_PinMux(LPC_IOCON,0,27,MD_PLN,IOCON_FUNC0);
			Chip_GPIO_SetPinDIRInput(LPC_GPIO,0,27);

			Chip_IOCON_PinMux(LPC_IOCON,0,28,MD_PLN,IOCON_FUNC0);
			Chip_GPIO_SetPinDIRInput(LPC_GPIO,0,28);

			Chip_IOCON_PinMux(LPC_IOCON,0,21,MD_PLN,IOCON_FUNC0);
			Chip_GPIO_SetPinDIRInput(LPC_GPIO,0,21);

			Chip_IOCON_PinMux(LPC_IOCON,0,22,MD_PLN,IOCON_FUNC0);
			Chip_GPIO_SetPinDIRInput(LPC_GPIO,0,22);

			/****************************************************************/


			Chip_GPIO_SetPinOutHigh(LPC_GPIO,MOTORX_D0);
			Chip_GPIO_SetPinOutHigh(LPC_GPIO,MOTORX_D1);
			Chip_GPIO_SetPinOutHigh(LPC_GPIO,MOTORX_D2);
			Chip_GPIO_SetPinOutHigh(LPC_GPIO,MOTORX_D3);



			Chip_GPIO_SetPinOutHigh(LPC_GPIO,MOTORY_D0);
			Chip_GPIO_SetPinOutHigh(LPC_GPIO,MOTORY_D1);
			Chip_GPIO_SetPinOutHigh(LPC_GPIO,MOTORY_D2);
			Chip_GPIO_SetPinOutHigh(LPC_GPIO,MOTORY_D3);




			Chip_GPIO_SetPinOutHigh(LPC_GPIO,MOTORZ_D0);
			Chip_GPIO_SetPinOutHigh(LPC_GPIO,MOTORZ_D1);
			Chip_GPIO_SetPinOutHigh(LPC_GPIO,MOTORZ_D2);
			Chip_GPIO_SetPinOutHigh(LPC_GPIO,MOTORZ_D3);


	Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORX_D0);
	Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORX_D1);
	Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORX_D2);
	Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORX_D3);



	Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORY_D0);
	Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORY_D1);
	Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORY_D2);
	Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORY_D3);




	Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORZ_D0);
	Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORZ_D1);
	Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORZ_D2);
	Chip_GPIO_SetPinOutLow(LPC_GPIO,MOTORZ_D3);


	Chip_GPIO_SetPinOutHigh(LPC_GPIO,MOTORX_E);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO,MOTORY_E);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO,MOTORZ_E);

	//set_motor(MOTORX,HORARIO ,0,0);
	//set_motor(MOTORY,HORARIO ,0,0);
	//set_motor(MOTORZ,HORARIO ,0,0);
	return 1;
	}

uint8_t push_buffer_CNC(uint32_t x,uint32_t y,uint32_t z)
{
	if(CNC_buffer_full) return 0; //lleno
	buffer_CNC[indice_CNC_in].steps_x=x;
	buffer_CNC[indice_CNC_in].steps_y=y;
	buffer_CNC[indice_CNC_in].steps_z=z;
	indice_CNC_in++;
	indice_CNC_in%=TOPE_BUFFER_CNC;
	CNC_BUFFER_EMPTY=0;
	if(indice_CNC_in==indice_CNC_out)
		CNC_buffer_full=1;
	return 1;
}

position_xyz_t* pop_buffer_CNC()
{
	position_xyz_t*dato;
	if(CNC_BUFFER_EMPTY) return NULL;
	dato=&(buffer_CNC[indice_CNC_out]);
	indice_CNC_out++;
	indice_CNC_out%=TOPE_BUFFER_CNC;
	CNC_buffer_full=0;
	if(indice_CNC_in==indice_CNC_out)
		CNC_BUFFER_EMPTY=1;
	return dato;
}

/*void TIMER3_IRQHandler(void)
{

	if (Chip_TIMER_MatchPending(LPC_TIMER3, 1))
	{
		Chip_TIMER_ClearMatch(LPC_TIMER3, 1);
		dr_rotar_motorx();
	}
}*/

void TIMER1_IRQHandler(void)
{

	if (Chip_TIMER_MatchPending(LPC_TIMER1, MOTORX))
	{
		Chip_TIMER_ClearMatch(LPC_TIMER1, MOTORX);
		dr_rotar_motorx();
		Chip_TIMER_SetMatch(LPC_TIMER1, MOTORX,Chip_TIMER_ReadCount(LPC_TIMER1)+ (timerFreq / (motor[MOTORX].vel*4)));

	}
	if (Chip_TIMER_MatchPending(LPC_TIMER1, MOTORY))
		{
		Chip_TIMER_ClearMatch(LPC_TIMER1, MOTORY);
		dr_rotar_motory();
			Chip_TIMER_SetMatch(LPC_TIMER1, MOTORY,Chip_TIMER_ReadCount(LPC_TIMER1)+ (timerFreq / (motor[MOTORY].vel*4)));

		}
	if (Chip_TIMER_MatchPending(LPC_TIMER1, MOTORZ))
		{
		Chip_TIMER_ClearMatch(LPC_TIMER1, MOTORZ);
		dr_rotar_motorz();
			Chip_TIMER_SetMatch(LPC_TIMER1, MOTORZ, Chip_TIMER_ReadCount(LPC_TIMER1)+ (timerFreq / (motor[MOTORZ].vel*4)));

		}
}

/*void TIMER2_IRQHandler(void)
{

	if (Chip_TIMER_MatchPending(LPC_TIMER2, 1))
	{
		Chip_TIMER_ClearMatch(LPC_TIMER2, 1);
		dr_rotar_motorz();
	}
}*/

uint8_t mover_motor(uint8_t n_motor, uint32_t position,uint32_t vel)
{

		if(motor[n_motor].steps == position) return 1;
		if(motor[n_motor]. steps> position)
		{
			set_motor(n_motor,HORARIO,vel,(motor[n_motor].steps - position));
		//	motor[n_motor].posicion_pasos-=(motor[n_motor].posicion_pasos - posicion);
		}
		else
		{
			set_motor(n_motor,AHORARIO,vel,(position - motor[n_motor].steps));

		}
		return 1;

}

/*inline uint8_t motor_flag_stop(uint8_t n_motor)

	{
	return motor[n_motor].flag_stop;
	}*/

void motor_set_zero(void)
	{
	motor[MOTORZ].steps=0;motor[MOTORX].steps=0;motor[MOTORY].steps=0;
	motor[MOTORZ].pos_mm=0;motor[MOTORX].pos_mm=0;motor[MOTORY].pos_mm=0;
	return ;
	}


