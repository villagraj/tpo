/*
 * chip_eint.c
 *
 *  Created on: 27 nov. 2018
 *      Author: Mauro
 */

#include "chip_eint.h"
void init_iexterna(void)
{
	EXTINT|=(0X07);

	NVIC_DisableIRQ(EINT1_IRQn);
	NVIC_DisableIRQ(EINT0_IRQn);
	NVIC_DisableIRQ(EINT2_IRQn);
	NVIC_ClearPendingIRQ(EINT1_IRQn);
	NVIC_ClearPendingIRQ(EINT0_IRQn);
	NVIC_ClearPendingIRQ(EINT2_IRQn);
	//Chip_IOCON_PinMux(LPC_IOCON, 2,13,MD_PUP,IOCON_FUNC1);
	Chip_IOCON_PinMux(LPC_IOCON, 2,12,MD_PUP,IOCON_FUNC1);
	Chip_IOCON_PinMux(LPC_IOCON, 2,11,MD_PUP,IOCON_FUNC1);
	Chip_IOCON_PinMux(LPC_IOCON, 2,10,MD_PUP,IOCON_FUNC1);

	EXTMODE|=(0x07);//las 4 iext generan interrupciones por NIVEL
	EXTPOLAR|=(0x07); // las iext por NIVEL BAJO

	EXTINT|=(0X07);
	NVIC_EnableIRQ(EINT1_IRQn);
	NVIC_EnableIRQ(EINT0_IRQn);
	NVIC_EnableIRQ(EINT2_IRQn);
	EXTINT|=(0X07);

	//NVIC_EnableIRQ(EINT3_IRQn);
}
