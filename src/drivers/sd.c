/*
 * sd.c
 *
 *  Created on: 28 feb. 2020
 *      Author: MV
 */
#include "ff.h"
#include "CAPI_diskio.h"
#include "sd.h"
#include "string.h"

FIL 	fp_log;
 FILINFO	fs_log;
 FATFS 	Fatfs_log[1];

void LeerConfigSD(void)
{
	char 	datos[64];
	char 	nombreArchivo[20] = "0:hola.txt";
	FATFS 	fs[1];
	FIL 	handleArchivo;
	FRESULT resultado;

	f_mount(0, &fs[0]);
	resultado = f_open(&handleArchivo, nombreArchivo, FA_OPEN_EXISTING | FA_READ);

	if(resultado == FR_OK)
	{
		f_gets(datos,64,&handleArchivo);
	}
	f_close(&handleArchivo);
	f_mount(0, NULL);
}

void EscribirArchivoSD(void)
{
	FIL 	fp;
	FATFS 	Fatfs[1];
	int 	bw;
	int 	len;
	char 	datos[16]="Anduvo Ok!!";
	len = strlen(datos);

	f_mount(0, &Fatfs[0]);
	f_open(&fp,"0:salida.txt",FA_OPEN_ALWAYS | FA_WRITE);
	f_write(&fp,(const void*)datos,len,(UINT*)&bw);
	f_sync(&fp);
	f_close(&fp);
	f_mount(0, NULL);

	f_mount(0, &Fatfs_log[0]);
	f_open(&fp_log,"0:log.txt",FA_OPEN_ALWAYS | FA_WRITE);
	f_stat("0:log.txt",&fs_log);
	f_lseek(&fp_log,fs_log.fsize);
	f_sync(&fp_log);
}



void TareaLogSD(void)
{

	static char val[3] = "0\r\n";
	int bw;
	f_write(&fp_log,(const void*)&val[0],3,(UINT*)&bw);
	f_sync(&fp_log);
	if(++val[0]>'9') val[0]= '0';
}

