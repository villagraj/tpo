/*
 * motor_pwm.c
 *
 *  Created on: 17 nov. 2018
 *      Author: Mauro
 */

#include "motor_pwm.h"
#include "pwm_17xx_40xx.h"

uint8_t init_motor_pwm()

{
	uint32_t frec,prescal;


	frec=Chip_Clock_GetSystemClockRate();
	prescal=frec/(100*1000);


	Chip_PWM_PrescaleSet(LPC_PWM1,prescal);


	Chip_IOCON_PinMux(LPC_IOCON,2,0,MD_PLN,IOCON_FUNC0);/*configuro pin P2.0 como salida pwm*/
	Chip_IOCON_EnableOD(LPC_IOCON,2,0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,2, 0);

	Chip_GPIO_SetPinOutLow(LPC_GPIO,2, 0);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO,2, 0);

	Chip_PWM_SetControlMode(LPC_PWM1,1, PWM_SINGLE_EDGE_CONTROL_MODE, PWM_OUT_DISABLED);

	Chip_PWM_SetMatch(LPC_PWM1,0,100);
	Chip_PWM_ResetOnMatchEnable(LPC_PWM1, 0);
	Chip_PWM_LatchEnable(LPC_PWM1,0,PWM_OUT_DISABLED);
	Chip_PWM_MatchEnableInt(LPC_PWM1, 0);

	Chip_PWM_ResetOnMatchDisable(LPC_PWM1,1);
	Chip_PWM_SetMatch(LPC_PWM1,1,50);
	Chip_PWM_LatchEnable(LPC_PWM1,1,PWM_OUT_DISABLED);
	Chip_PWM_MatchEnableInt(LPC_PWM1, 1);

	//Chip_PWM_ResetOnMatchDisable(LPC_PWM1,2);
		//Chip_PWM_SetMatch(LPC_PWM1,2,0);
	//	Chip_PWM_LatchEnable(LPC_PWM1,2,PWM_OUT_ENABLED);
		//Chip_PWM_MatchEnableInt(LPC_PWM1, 2);

	Chip_PWM_MatchDisableInt(LPC_PWM1, 2);
	Chip_PWM_MatchDisableInt(LPC_PWM1, 3);
	Chip_PWM_MatchDisableInt(LPC_PWM1, 4);
	Chip_PWM_MatchDisableInt(LPC_PWM1, 5);
	Chip_PWM_MatchDisableInt(LPC_PWM1, 6);

	Chip_PWM_ClearMatch(LPC_PWM1,0);
	Chip_PWM_ClearMatch(LPC_PWM1,1);
	Chip_PWM_ClearMatch(LPC_PWM1,2);


	return 1;

}

uint8_t vel_motor_pwm(uint8_t duty)
{
	Chip_PWM_SetMatch(LPC_PWM1,1,duty);
	Chip_PWM_LatchEnable(LPC_PWM1,1,PWM_OUT_ENABLED);
	return 1;
}

uint8_t stop_motor_pwm()
{
	Chip_PWM_Disable(LPC_PWM1);
	NVIC_DisableIRQ(PWM1_IRQn);
	return 1;
}

uint8_t start_motor_pwm()
{
	Chip_PWM_ClearMatch(LPC_PWM1,1);
	Chip_PWM_ClearMatch(LPC_PWM1,0);
	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_PWM1);
	Chip_PWM_Enable(LPC_PWM1);
	Chip_PWM_Reset(LPC_PWM1);
	NVIC_ClearPendingIRQ(PWM1_IRQn);
	NVIC_EnableIRQ(PWM1_IRQn);

	return 1;
}

void PWM1_IRQHandler(void)
	{
	if(Chip_PWM_MatchPending(LPC_PWM1,0)==TRUE)
		{
		Chip_PWM_ClearMatch(LPC_PWM1,0);
		Chip_GPIO_SetPinOutHigh(LPC_GPIO, 2, 0);
		return;
		}
	if(Chip_PWM_MatchPending(LPC_PWM1,1)==TRUE)
			{
			Chip_PWM_ClearMatch(LPC_PWM1,1);
			Chip_GPIO_SetPinOutLow(LPC_GPIO, 2, 0);
			return;
			}
	//Chip_PWM_ClearMatch(LPC_PWM1,0);
	return;
	}
