/*
 * motor_pap_task.h
 *
 *  Created on: 13 dic. 2018
 *      Author: Mauro
 */

#ifndef MY_PROYECT_INC_MOTOR_PAP_TASK_H_
#define MY_PROYECT_INC_MOTOR_PAP_TASK_H_

void vTask_motor_pap_init_hw(void*);
void vTask_motor_pap_init_cnc(void*);
uint8_t create_sema(void);
uint8_t create_task_pap();

#endif /* MY_PROYECT_INC_MOTOR_PAP_TASK_H_ */
