/*!\file
 *En este header se encuentran prototipos de funciones macros y tipo de datos necesarios
 *para el manejo de los motores paso a paso
 */


#ifndef EXAMPLE_INC_MOTOR_PAP_C_

#include "chip.h"

#define EXAMPLE_INC_MOTOR_PAP_C_

#define HORARIO 	1 /*!<sentido de giro horario*/
#define AHORARIO 	0/*!<sentido de giro anti horario*/

#define MOTORY	2
#define MOTORX 	1
#define MOTORZ	0

#define S_STEPS 0
#define D_STEPS 1
#define H_STEPS 2

#define MOTORX_E 1,22	/*!<enable motor x*/
#define MOTORY_E 1,19	/*!<enable motor y*/
#define MOTORZ_E 4,29	/*!<enable motor y*/

#define MASK_MOTORX 0xFFFFFFF0
#define MASK_MOTORY 0xFFFFFF0F
#define MASK_MOTORZ 0xFE1FFFFF

#define MOTORX_D0 0,6

#define MOTORX_D1 0,7
#define MOTORX_D2 0,8
#define MOTORX_D3 0,9

#define MOTORY_D0 0,1
#define MOTORY_D1 0,0
#define MOTORY_D2 1,26	//pad 9
#define MOTORY_D3 1,20	//pad12

#define MOTORZ_D0 0,4
#define MOTORZ_D1 0,5
#define MOTORZ_D2 0,10
#define MOTORZ_D3 0,11

#define TOP_X 150			/*!<quince centimetros maximos de desplazamiento en x*/
#define TOP_Y 150			/*!<quince centimetros maximos de desplazamiento en y*/
#define TOP_Z 30			/*!<3 centimetros maximos de desplazamiento*/
#define MAX_VELOCITY 3000 	/*!<cantidad maxima de pasos por segundo*/


#define POS_X (motor[MOTORX].posicion_pasos/26)/*!<posicion en milimetros motor x*/
#define POS_Y (motor[MOTORY].posicion_pasos/26)/*!<posicion en milimetros motor y*/
#define POS_Z (motor[MOTORZ].posicion_pasos/26)/*!<posicion en milimetros motor z*/

#define MAX_MOTOR 3		/*!<cantidad maxima de motores*/
#define PASOS_MAX 4500 	/*!<tope en pasos*/

#define TOPE_BUFFER_CNC 1000


/*! \brief
*Estructura que maneja los motores paso a paso
*
*/
typedef struct
	{
	uint8_t		steps_i;		/*!< indice de pasos */
	uint8_t 	direction;		/*!< direccion de giro horario o anti horario*/
	uint32_t 	steps_to_do;	/*!< cantidad de pasos que quedan por realizar*/
	uint32_t   	steps;			/*!< posicion actual del motor en pasos*/
	uint8_t 	flag_stop;		/*!< bandera que indica si el motor esta detenido*/
	uint32_t 	vel;
	uint32_t    pos_mm;			/*!< posición en milimetros del motor*/
	}	stepper_m_t;

	extern stepper_m_t motor[3];

/*! \brief
*Buffer principal de motores
*/

	/*! \brief
	*Estructura que permite cargar a donde se deben mover los motores
	*/
	typedef struct posicion_xyz
		{
			uint32_t steps_x;		/*!< pasos a realizar en x*/
			uint32_t steps_y;		/*!< pasos a realizar en y*/
			uint32_t steps_z;		/*!<pasos a realizar en z*/
		} position_xyz_t;
/*! \brief
*	funcion que permite indicar que motor,con que sentido de giro,a que velocidad y cuantos pasos debera moverse.
*
*/
uint8_t	set_motor(uint8_t n_motor,uint8_t direction ,uint32_t vel,uint32_t steps);
/*! \brief
*	Funcion de bajo nivel encargada de hacer cada paso (debe llamarse dentro de una interrupcion periodica.
*
*/
void dr_rotar_motor();
/*! \brief
*	Inicializacion de puertos y timers utilizados por los motores.
	\param sdh tipo de paso a realizar 0 paso simple,1 paso doble,2 medio paso
*/
uint8_t init_motor(uint8_t sdh);
/*! \brief
*	Funcion de mas alto nivel que set_motor esta indica a que picision (en pasos) debe moverse el motor y a que velocidad.
 	\param n_motor motor a mover.
	\param position posicion a moverse.
	\param vel velocidad del motor.
	\return retorna OK.
	\sa set_motor(uint8_t ,uint8_t direction ,uint32_t vel,uint32_t steps),dr_rotar_moto(void);
*/
uint8_t mover_motor(uint8_t n_motor, uint32_t position,uint32_t vel);

/*! \brief
*	Funcion que saca del buffer_CNC una coordenada a realizar .
	\return retorna la primera coodenada agregada al buffer_CNC[].
*/
position_xyz_t* pop_buffer_CNC();
/*! \brief
*	Funcion que agrega al buffer_CNC una coordenada a realizar.
	\param x coordenada en x.
	\param y coordenada en y.
	\param z coordenada en z.
	\return retorna OK.
*/
uint8_t push_buffer_CNC(uint32_t x,uint32_t y ,uint32_t z);
/*! \brief
*	Funcion que inicia el timer que controla los motores
*/
uint8_t init_motor_timer();
/*! \brief
*	Funcion que detiene un motor
*/
void stop_motor(uint8_t n_motor);
/*! \brief
*	Funcion que continua haciendo los pasos del motor si no estaba detenido no tiene efecto
*/
void continue_motor(uint8_t n_motor);
/*! \brief
*	funcion que lee si se detubo el motor
*/
//uint8_t motor_pap_getstop(uint8_t nmotor);

STATIC INLINE uint8_t motor_pap_getstop(uint8_t nmotor)
{
	return(motor[nmotor].flag_stop);
}
/*! \brief
*	funcion que retorna pos en pasos del motor
*/
STATIC INLINE uint8_t motor_pap_getsteps(uint8_t nmotor)
{
	return(motor[nmotor].steps);
}

void motor_set_zero(void);


#else
#endif /* EXAMPLE_INC_MOTOR_PAP_C_ */


