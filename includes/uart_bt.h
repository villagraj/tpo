/*
 * uart_bt.h
 *
 *  Created on: 23 dic. 2018
 *      Author: Mauro
 */

#ifndef MY_PROYECT_INC_UART_BT_H_
#define MY_PROYECT_INC_UART_BT_H_
#include "chip.h"
#define UART_SELECTION 	LPC_UART3
#define IRQ_SELECTION 	UART3_IRQn
#define HANDLER_NAME 	UART3_IRQHandler

/* Transmit and receive ring buffer sizes */
#define UART_SRB_SIZE 200	/* Send */
#define UART_RRB_SIZE 200	/* Receive */

#define cmd_reset 0x00
#define cmd_start_file 0x01 //avisa que luego de esto recibira coordenadas
#define cmd_end_file 0x02
#define cmd_cordenada 0x55
#define cmd_punto 0x03 //avisa que solo realiza un posicionamiento, para cuando termine se posicione en el 0,0,0
#define cmd_pausa 0x04
#define cmd_init_z 0x05
#define cmd_man_x 0x06
#define cmd_man_y 0x07
#define cmd_man_z 0x08
#define TRAMA_OK 0X10
#define cmd_ERROR 0X11

#define TXD3 0,0
#define RXD3 0,1

typedef enum estado_analizar_rx{RX_start=0,RX_comando,RX_x,RX_y,RX_z,RX_paridad,RX_fin}estado_analizar_t;

uint8_t init_uart_bt(void);
void vTask_uart_bt(void*vparam);
uint8_t analisis_de_comando_bt(uint8_t,uint8_t,uint8_t,uint8_t);

#endif /* MY_PROYECT_INC_UART_BT_H_ */
