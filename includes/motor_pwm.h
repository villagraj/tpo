/*!\file
 *En este header se encuentran prototipos de funciones macros y tipo de datos necesarios
 *para el manejo del motor de corriente continua encargado de manejar la herramienta
 */

#ifndef MY_PROYECT_INC_MOTOR_PWM_H_
#define MY_PROYECT_INC_MOTOR_PWM_H_

#include "chip.h"
#include "pwm_17xx_40xx.h"

#define PWM1_1 2,0
#define MOTOR_CC_F 10000

uint8_t init_motor_pwm();
uint8_t stop_motor_pwm();
uint8_t start_motor_pwm();
uint8_t shutdown_motor_pwm();
uint8_t vel_motor_pwm(uint8_t );


#endif /* MY_PROYECT_INC_MOTOR_PWM_H_ */
