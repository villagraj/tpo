

#ifndef _BOTO_H_
#define _BOTO_H_



//defines
#define BOT_IZQ 1,30 // shunt y
#define BOT_DER 1,31 //shunt pwm
#define BOT_OK	0,2  //shunt z
#define ON 1
#define OFF 0
#define MAX_DEB 200
#define DEBOUNCE_MAX 10

typedef enum {IZQ, DER, OK, NADA} boton;
typedef enum {BOTON_A,BOTON_B,BOTON_C} menu;





//prototipos

void Init_Botonera(void);
void Task_Debounce();
#endif
