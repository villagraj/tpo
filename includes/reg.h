#ifndef REG_H_
#define REG_H_

typedef unsigned int u32_t;
typedef unsigned short u16_t;
typedef unsigned char u8_t;
#define		__R					volatile const
#define		__W					volatile
#define		__RW				volatile

typedef __RW u32_t register_t;


//estructura de manejo de GPIO
typedef struct{
	u32_t FIODIR;							//SOLO VALIDO PARA GPIO-1 ES SALIDA, 0 ENTRADA
	u32_t RESERVED[3];
	u32_t FIOMASK;
	u32_t FIOPIN;							//SETEAR O CONSULTAR VALOR DE PIN
	u32_t FIOSET;							//PONER A 1 UN PIN
	u32_t FIOCLR;							//PONER A CERO UN PIN
	}gpio_t;

//estructura Systick
typedef struct
{
	union
	{
		register_t _STCTRL;
		struct
		{
			register_t _ENABLE:1;
			register_t _TICKINT:1;
			register_t _CLKSOURCE:1;
			register_t _RESERVED:13;
			register_t _COUNTFLAG:1;
			register_t _RESERVED1:15;
		};
	};
	register_t _STRELOAD;
	register_t _STCURR;
	union
	{
		register_t STCALIB;
		struct
		{
			register_t _TENMS:24;
			register_t _RESERVED2:6;
			register_t _SKEW:1;
			register_t _NOREF:1;
		};
	};
}systick_t;

#define systick ((systick_t *) 0XE000E010UL)

#define STCTRL systick->_STCTRL
#define ENABLE systick->_ENABLE
#define TICKINT systick->_TICKINT
#define CLKSOURCE systick->_CLKSOURCE
#define COUNTFLAG systick->_COUNTFLAG
#define STRELOAD systick->_STRELOAD
#define STCURR systick->_STCURR
#define STCALIB systick->_STCALIB
#define SKEW systick->_SKEW
#define NOREF systick->_NOREF
#define TENMS systick->_TENMS

/******************************************************************************************************/
#define PINSEL ( (register_t *) 0x4002C000UL) //FUNCION DEL PIN (GPIO, ADC,UART IEXT...)
#define GPIO ((gpio_t *) 0x2009C000UL)
#define PINMODE ((register_t *) 0x4002C040UL) //REGISTRO PARA PULLDOWN,PULLUP...

#define		ISER		( ( register_t  * ) 0xE000E100UL )
//0xE000E180UL : Direccion de inicio de los registros de deshabilitacion (clear) de interrupciones en el NVIC:
#define		ICER		( ( register_t * ) 0xE000E180UL )
#define		ICPR		( ( register_t * ) 0xE000E280UL ) //registro de borrado interrupciones pendientes
//Registros ISER:
#define		ISER0		ISER[0]
#define		ISER1		ISER[1]

//Registros ICER:
#define		ICER0		ICER[0]
#define		ICER1		ICER[1]

//Registros ICPR:
#define		ICPR0		ICPR[0]
#define		ICPR1		ICPR[1]
/******************************************************************************************************/
#define OUTPUT 1
#define INPUT 0

#define ON 1
#define OFF 0
/*******************************************************************************************************/
#define PINSEL_GPIO 0
#define PINSEL_FUN1 1
#define PINSEL_FUN2 2
#define PINSEL_FUN3 3

#define PINMODE_PULLUP 0
#define PINMODE_REPEAT 1
#define PINMODE_NONE 2
#define PINMODE_PULLDOWN 3
/********************************************************************************************************/
#define 	PCONP	(* ( ( register_t  * ) 0x400FC0C4UL ))//ENCENDER O PERIFERICO POWER CONTROL PERIFERICAL
#define		PCLKSEL		( ( register_t  * ) 0x400FC1A8UL ) //CONTROL DE VELOCIDADES DE LOS PERIFERICOS

#define		PCLKSEL0	PCLKSEL[0] //CONTROL DE VELOCIDAD RELOJ PERIFERICOS 1/2 PAG 57
#define		PCLKSEL1	PCLKSEL[1] //CONTROL DE VELOCIDAD RELOJ PERIFERICOS 2/2 PAG 57
/*******************************************************************************************************/
/*UART0:************************************************************************************************/
//0x4001000CUL : Registro de control de la UART0:
#define		DIR_U0FCR	( ( register_t   * ) 0x4000C008UL )
//0x4001000CUL : Registro de control de la UART0:
#define		DIR_U0LCR	( ( register_t  * ) 0x4000C00CUL )
//0x40010014UL : Registro de recepcion de la UART0:
#define		DIR_U0LSR		( ( register_t   * ) 0x4000C014UL )
//0x40010000UL : Parte baja del divisor de la UART0:
#define		DIR_U0DLL	( ( register_t   * ) 0x4000C000UL )
//0x40010004UL : Parte alta del divisor de la UART0:
#define		DIR_U0DLM	( ( register_t   * ) 0x4000C004UL )
//0x40010000UL : Registro de recepcion de la UART0:
#define		DIR_U0RBR	( ( register_t   * ) 0x4000C000UL )
//0x40010000UL : Registro de transmision de la UART0:
#define		DIR_U0THR	( ( register_t   * ) 0x4000C000UL )
/*interupcion pendiente*/
#define		DIR_U0IIR	( ( register_t   * ) 0x4000C008UL )

//Registros de la UART0:
#define		U0THR		DIR_U0THR[0]
#define		U0RBR		DIR_U0RBR[0]
#define		U0LCR		DIR_U0LCR[0]
#define		U0LSR		DIR_U0LSR[0]
#define		U0DLL		DIR_U0DLL[0]
#define		U0DLM		DIR_U0DLM[0]
#define		U0FCR		DIR_U0FCR[0]
#define		U0IER		DIR_U0DLM[0]
#define		U0IIR		DIR_U0IIR[0]
/*********************************************************************************************************/
/*UART1:************************************************************************************************/
//0x4001000CUL : Registro de control de la UART0:
#define		DIR_U1FCR	( ( register_t   * ) 0x40010008UL )
//0x4001000CUL : Registro de control de la UART0:
#define		DIR_U1LCR	( ( register_t  * ) 0x4001000CUL )
//0x40010014UL : Registro de recepcion de la UART0:
#define		DIR_U1LSR		( ( register_t   * ) 0x40010014UL )
//0x40010000UL : Parte baja del divisor de la UART0:
#define		DIR_U1DLL	( ( register_t   * ) 0x40010000UL )
//0x40010004UL : Parte alta del divisor de la UART0:
#define		DIR_U1DLM	( ( register_t   * ) 0x40010004UL )
//0x40010000UL : Registro de recepcion de la UART0:
#define		DIR_U1RBR	( ( register_t   * ) 0x40010000UL )
//0x40010000UL : Registro de transmision de la UART0:
#define		DIR_U1THR	( ( register_t   * ) 0x40010000UL )
/*interupcion pendiente*/
#define		DIR_U1IIR	( ( register_t   * ) 0x40010008UL )

//Registros de la UART0:
#define		U1THR		DIR_U1THR[0]
#define		U1RBR		DIR_U1RBR[0]
#define		U1LCR		DIR_U1LCR[0]
#define		U1LSR		DIR_U1LSR[0]
#define		U1DLL		DIR_U1DLL[0]
#define		U1DLM		DIR_U1DLM[0]
#define		U1FCR		DIR_U1FCR[0]
#define		U1IER		DIR_U1DLM[0]
#define		U1IIR		DIR_U1IIR[0]
/*********************************************************************************************************/
// Interrupciones Externas
//-----------------------------------------------------------------------------
	#define		EXTINT_ 		( ( register_t  * ) 0x400FC140UL )
	#define		EXTINT		EXTINT_[0]

	#define		EXTMODE_ 	( ( register_t  * ) 0x400FC148UL )
	#define		EXTMODE		EXTMODE_[0]

	#define		EXTPOLAR_ 	( ( register_t  * ) 0x400FC14CUL )
	#define 	EXTPOLAR	EXTPOLAR_[0]

	#define EINT0  0
	#define EINT1  1
	#define EINT2  2
	#define EINT3  3
//--------------------------------------------------------------------------
#endif
