/*--------------------------------------------------------------------*-

    task-car_wash_controller_lpc1769.h (Released 2019-04)

--------------------------------------------------------------------

    - See task-car_wash_controller_lpc1769.c for details.

-*--------------------------------------------------------------------*/


#ifndef _CAR_WASH_CONTROLLER_H
#define _CAR_WASH_CONTROLLER_H 1


// ------ Public constants -----------------------------------------


// ------ Public data type declarations ----------------------------
typedef enum {INICIO_Z,INICIO_XY,INICIO_FIN} estado_inicio;
typedef enum {CNC_standby, CNC_inicializacion,CNC_calibracion_z,CNC_fresadora,CNC_CARGAR,CNC_movimiento, CNC_perforacion} estado_cnc;
typedef enum {CNC_xy, CNC_mov_z_down, CNC_mov_z_up} estado_perforacion;
extern estado_cnc ESTADO_CNC;


// ------ Public function prototypes -------------------------------

void Task_Perforacion(void);
void Task_Init(void);
void Task_calibracion_z(void);
void Task_fresadora(void);
void Task_CargarArchivo(void);


#endif

/*------------------------------------------------------------------*-
  ---- END OF FILE -------------------------------------------------
-*------------------------------------------------------------------*/
