/*
 * chip_eint.h
 *
 *  Created on: 27 nov. 2018
 *      Author: Mauro
 */

#ifndef MY_PROYECT_INC_CHIP_EINT_H_
#define MY_PROYECT_INC_CHIP_EINT_H_
#include "chip.h"
// Interrupciones Externas
//-----------------------------------------------------------------------------
	#define		EXTINT_ 		( (__IO uint32_t * ) 0x400FC140UL )
	#define		EXTINT		EXTINT_[0]

	#define		EXTMODE_ 	( ( __IO uint32_t * ) 0x400FC148UL )
	#define		EXTMODE		EXTMODE_[0]

	#define		EXTPOLAR_ 	( ( __IO uint32_t * ) 0x400FC14CUL )
	#define 	EXTPOLAR	EXTPOLAR_[0]

	#define EINT0  0
	#define EINT1  1
	#define EINT2  2
	#define EINT3  3
//--------------------------------------------------------------------------

void init_iexterna();
#endif /* MY_PROYECT_INC_CHIP_EINT_H_ */
