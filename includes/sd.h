#ifndef _SD_H_
#define _SD_H_

#include "ff.h"
#include "CAPI_diskio.h"

void InitHardware(void);
void TareaTickLed(void);
void TareaLogSD(void);
void LeerSD(void);
void EscribirArchivoSD(void);


#endif
